class UploadsController < ApplicationController
  before_action :set_upload, only: [:show, :edit, :update, :destroy, :download, :upvote, :verify]
  before_action :authenticate_user!, except: [:index, :search]
  before_action :is_owner, only: [:edit, :update, :destroy]
  before_action :college_set, only: [:create, :destroy, :edit]

  def index
    unless user_signed_in?
      redirect_to new_user_registration_path
    end
    @uploads = Upload.all
    @uploader = Upload.new.link
    @uploads = @uploads.paginate(page: params[:page], per_page: 10) unless @uploads.empty?
    if Rails.env == 'development'
      @uploader.success_action_redirect = "http://localhost:3003/uploads/new"
    elsif Rails.env == 'production' 
      @uploader.success_action_redirect = "http://www.collegeshare.in/uploads/new"
    end
  end

  def search
    @uploads = Upload.search(params[:query])
    @uploads = @uploads.paginate(page: params[:page], per_page: 10) unless @uploads.empty? or @uploads.empty?
  end

  def download
    @download = @upload.downloads.create! user_id: current_user.id
    if @download and @upload.update_attributes!(no_of_downloads: @upload.downloads.count)
      render status: 200, json: @upload
    else
      render status: 422, json: {message: "Upload could not be downloaded!!"}
    end
  end

  def upvote
    @upvote = @upload.upload_upvotes.create! user_id: current_user.id
    if @upvote and @upload.update_attributes!(upvotes: @upload.upload_upvotes.count)
      render status: 200, json: @upload
    else
      render status: 422, json: {message: "Upload could not be upvoted!"}
    end
  end

  def verify
    if @upload and @upload.update_attributes!(verified: !@upload.verified)
      render status: 200, json: @upload
    else
      render status: 422, json: {message: "Upload's verification could not be upadted!"}
    end
  end

  def show
  end

  def new
    @upload = Upload.new(key: params[:key])
  end


  def edit

  end

  def create
    @upload = Upload.new(upload_params)
    @upload.user_id = current_user.id
      if @upload.save
        redirect_to upload_path(id: @upload.id), notice: 'File has been successfully uploaded!'
      else
        redirect_to :back, alert: 'Upload was not saved!!'
      end
  end

  def update
    respond_to do |format|
      if @upload.update(upload_update_params)
        format.html { redirect_to upload_path(id: @upload.id), notice: 'Upload was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @upload.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @upload.destroy
    respond_to do |format|
      format.html { redirect_to uploads_url }
      format.json { head :no_content }
    end
  end

  private
    def set_upload
      @upload = Upload.find(params[:id])
    end

    def upload_params
      params.require(:upload).permit(:name, :description, :key)
    end

    def upload_update_params
      params.require(:upload).permit(:name, :description, :upvotes, :no_of_downloads, :key)
    end
end
