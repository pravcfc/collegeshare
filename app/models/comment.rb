class Comment < ActiveRecord::Base
	belongs_to :user
	belongs_to :owner, polymorphic: true
	validates :content, presence: true, length: {minimum: 1}, allow_blank: false
end
