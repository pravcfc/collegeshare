class CreateDownloads < ActiveRecord::Migration
  def change
    create_table :downloads do |t|
      t.integer :user_id
      t.integer :upload_id

      t.timestamps
    end
    add_index :downloads, [:user_id, :upload_id], unique: true
  end
end
