class CreateUploadUpvotes < ActiveRecord::Migration
  def change
    create_table :upload_upvotes do |t|
      t.integer :user_id
      t.integer :upload_id

      t.timestamps
    end
    add_index :upload_upvotes, [:user_id, :upload_id], unique: true
  end
end
