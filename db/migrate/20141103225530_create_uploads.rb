class CreateUploads < ActiveRecord::Migration
  def change
    create_table :uploads do |t|
      t.string :name
      t.text :description
      t.integer :user_id
      t.string :link
      t.boolean :upload_processed, default: false

      t.timestamps
    end
  end
end
