Collegeshare::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # Do not eager load code on boot. This avoids loading your whole application
  # just for the purpose of running a single test. If you are using a tool that
  # preloads Rails for running tests, you may have to set it to true.
  config.eager_load = false

  # Configure static asset server for tests with Cache-Control for performance.
  config.serve_static_assets  = true
  config.static_cache_control = "public, max-age=3600"

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  ENV['BUCKET_NAME'] = 'cshare1'
  ENV['ACCESS_KEY_ID'] = 'AKIAJI5DG4DQQV5GEFAQ'
  ENV['SECRET_ACCESS_KEY'] = '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'

  ENV['GOOGLE_CLIENT_ID'] = '947088891346-gugotjd3ebinlorfrk1gs026mc6cfjt7.apps.googleusercontent.com'
  ENV['GOOGLE_CLIENT_SECRET'] = 'uVUYx-CubA49YKuQJJ5AkOH7'

  ENV['FACEBOOK_APP_ID'] = '808365075863583'
  ENV['FACEBOOK_SECRET'] = 'd5b4464fb91c4e20e8d0676974fd4c10'

  ENV['TWITTER_CONSUMER_ID'] = 'i7k77EfbPZ32Dsydn7wKuDbTW'
  ENV['TWITTER_CONSUMER_KEY'] = 'J6kk0kA4QFv59UBe4bfs8biZ8cremi8b15TlEcIhGD4J5Iq3QJ'

  # Raise exceptions instead of rendering exception templates.
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment.
  config.action_controller.allow_forgery_protection = false

  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

  # Print deprecation notices to the stderr.
  config.active_support.deprecation = :stderr
end
