class ChangeIndexingToNonUnique < ActiveRecord::Migration
  def change
  	remove_index :downloads, [:user_id, :upload_id]
  	remove_index :upload_upvotes, [:user_id, :upload_id]

  	add_index :downloads, [:user_id, :upload_id]
  	add_index :upload_upvotes, [:user_id, :upload_id]
  end
end
