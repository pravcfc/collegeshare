class AddDownloadsToUpload < ActiveRecord::Migration
  def change
  	add_column :uploads, :downloads, :integer, default: 0
  	rename_column :uploads, :upvote, :upvotes
  end
end
