class CollegesController < ApplicationController
	 before_action :authenticate_user!
	 before_action :is_admin

	def index
		@colleges = College.all
	end

	def new
		@college = College.new
	end

	def create
		@college = College.new(name_param)
		if @college.save
			redirect_to root_path, notice: 'College created succesfully!'
		else
			redirect_to root_path, alert: 'College was not created successfully!'
		end
	end

	def destroy
		@college = College.find(params[:id])
		if @college.delete
			redirect_to root_path, notice: 'College deleted succesfully!'
		else
			redirect_to root_path, alert: 'College was not deleted!'
		end
	end

	private
	def name_param
		params.require(:college).permit(:name)
	end
end
