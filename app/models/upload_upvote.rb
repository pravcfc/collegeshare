class UploadUpvote < ActiveRecord::Base
	belongs_to :user
	belongs_to :upload
	validates_presence_of :user_id, :upload_id
	validates :user_id, uniqueness: {scope: :upload_id}
end
