$(document).ready(function(){
		  

	  $('#new_upload_uploader').submit(function(){
	     valid = true;

	     if($("#file_upload").val() == ''){
	        alert('No file selected for upload!!');
	        valid =  false;
	    }

	    return valid
		});

		$('.download-button').click(function(){
			var card_id = $(this).parents('.collections-post.news-feed').attr('id');
			$.ajax({
			      async: false,
			      url: '/uploads/download/' + card_id,
			      data: {
			         format: 'json'
			      },
			      error: function() {
			       console.log('load error!!');
			      },
			      success: function(upload) {
			      		$('.collections-post.news-feed#'+card_id).find('.no-of-downloads').text(upload.no_of_downloads);
			      },
			      type: 'GET'
			   });
		});

		$('.upvote-button').click(function(){
			var card_id = $(this).parents('.collections-post.news-feed').attr('id');
			$.ajax({
			      async: false,
			      url: '/uploads/upvote/' + card_id,
			      data: {
			         format: 'json'
			      },
			      error: function() {
			       console.log('load error!!');
			      },
			      success: function(upload) {
			      	$('.collections-post.news-feed#'+card_id).find('.no-of-upvotes').text(upload.upvotes);
			      },
			      type: 'GET'
			   });
		});

});
