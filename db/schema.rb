# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151013123107) do

  create_table "colleges", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "owner_id"
    t.string   "owner_type"
  end

  add_index "comments", ["owner_id"], name: "index_comments_on_owner_id"
  add_index "comments", ["user_id"], name: "index_comments_on_user_id"

  create_table "downloads", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "upload_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "downloads", ["user_id", "upload_id"], name: "index_downloads_on_user_id_and_upload_id"

  create_table "relations", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "relations", ["followed_id"], name: "index_relations_on_followed_id"
  add_index "relations", ["follower_id", "followed_id"], name: "index_relations_on_follower_id_and_followed_id", unique: true
  add_index "relations", ["follower_id"], name: "index_relations_on_follower_id"

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "upload_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id"
  add_index "taggings", ["upload_id"], name: "index_taggings_on_upload_id"

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tags", ["id"], name: "index_tags_on_id"

  create_table "upload_upvotes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "upload_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "upload_upvotes", ["user_id", "upload_id"], name: "index_upload_upvotes_on_user_id_and_upload_id"

  create_table "uploads", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "user_id"
    t.string   "link"
    t.boolean  "upload_processed", default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "upvotes",          default: 0
    t.integer  "no_of_downloads",  default: 0
    t.boolean  "verified",         default: false
  end

  add_index "uploads", ["id", "user_id"], name: "index_uploads_on_id_and_user_id", unique: true
  add_index "uploads", ["name", "description"], name: "index_uploads_on_name_and_description"
  add_index "uploads", ["name", "user_id", "description", "id"], name: "index_uploads_on_name_and_user_id_and_description_and_id", unique: true
  add_index "uploads", ["user_id"], name: "index_uploads_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.integer  "college_id"
    t.boolean  "admin",                  default: false
    t.string   "uid"
    t.string   "provider"
    t.string   "profile_pic"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
