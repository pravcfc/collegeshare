class HomeController < ApplicationController
	#before_action :correct_user, only: [:edit, :update]

	def profile
		unless current_user
			redirect_to root_path, alert: 'You are not signed in!!'
		end
		@user = User.find params[:id]
		@uploads = @user.uploads.paginate(page: params[:page], per_page: 10) unless @user.uploads.empty?
	end

	def users
	end

	def following 
	  @user = User.find(params[:id])
	  @following = @user.following
	end

	def followers 
	  @user = User.find(params[:id])
	  @followers = @user.followers
	end

	def edit
		unless current_user
			redirect_to root_path, alert: 'You are not signed in!!'
		end
		@user = current_user
	end

	def update
		@user = User.find params[:user][:id]
		if @user.update_attributes(user_params)
			redirect_to root_path, notice: 'Profile has been succesfully updated!!'
		else
			redirect_to :back, alert: 'Profile changes were not saved due to some error!!'
		end
	end

	private
	def user_params
		params.require(:user).permit(:name, :college_id)
	end

	def correct_user
	  @user = User.find(params[:id]) if params[:id]
	  redirect_to root_url , alert: 'Cannot perform this action!!' unless current_user == @user
	end
end
