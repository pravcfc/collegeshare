Collegeshare::Application.routes.draw do

  if Rails.env.production?
    offline = Rack::Offline.configure :cache_interval => 120 do      
      cache ActionController::Base.helpers.asset_path("application.css")
      cache ActionController::Base.helpers.asset_path("application.js")
      # cache other assets
      network "/"  
    end
    match "/application.manifest" => offline, via: [:get, :post]
  end

  root to: 'uploads#index'
  get '/search', to: 'uploads#search'
  get 'welcome', to: 'home#welcome'
  get 'profile/:id', to: 'home#profile', as: 'profile'
  get 'edit', to: 'home#edit'
  match 'edit', to: 'home#update', via: :post
  get 'following/:id', to: 'home#following', as: 'following'
  get 'followers/:id', to: 'home#followers', as: 'followers'
  get 'users', to: 'home#users'
  post 'follow', to: 'relations#create_relation' 
  post 'unfollow', to: 'relations#destroy_relation'
  devise_for :users, controllers: {registrations: "registrations", omniauth_callbacks: "users/omniauth_callbacks"}
	
  resources :uploads do 
    resources :comments
    collection do 
      get '/upvote/:id', to: 'uploads#upvote'
      get '/download/:id', to: 'uploads#download'
      get '/verify/:id', to: 'uploads#verify'
    end
  end

  resources :colleges do 
    resources :college_term
  end

  resources :relations, only: [:create, :destroy]
end
