class Download < ActiveRecord::Base
	belongs_to :user
	belongs_to :upload
	validates :user_id, uniqueness: {scope: :upload_id}
end
