class Upload < ActiveRecord::Base
	validates_presence_of :name, :description, :user_id
	mount_uploader :link, UploadUploader
	default_scope { order(no_of_downloads: :desc, upvotes: :desc) }
	has_many :comments, as: :owner, dependent: :destroy
	has_many :upload_upvotes, dependent: :destroy
	# has_many :taggings, dependent: :destroy
	has_many :downloads, dependent: :destroy
  	# has_many :tags, through: :taggings
  	belongs_to :user
  	#after_save :create_taggings
	#after_save :enqueue_upload

	def filename
		File.basename(link.path || link.filename) if link
	end

	def self.search(query)
		Upload.where('lower(name) LIKE :query OR lower(description) LIKE :query', query: "%#{query.downcase}%")
	end

	def save_upload_params
		@upload.key = params[:key]
    	@upload.remote_link_url = @upload.link.direct_fog_url(with_path: true)
    	@upload.upload_processed = true
    	@upload.save!
	end

	def enqueue_upload
		UploadWorker.perform_async(id, key) if key.present?
	end

	class UploadWorker
		include Sidekiq::Worker

		def perform(id, key)
			upload = Upload.find(id)
			upload.key = key
			upload.remote_link_url = upload.link.direct_fog_url(with_path: true)
			upload.upload_processed = true
			upload.save!
		end
	end
end
