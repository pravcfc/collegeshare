CarrierWave.configure do |config|
	config.fog_credentials = {
			provider: 'AWS', 
			aws_access_key_id: ENV['ACCESS_KEY_ID'],
			aws_secret_access_key: ENV['SECRET_ACCESS_KEY'],
			region: 'ap-southeast-1'
	}
	config.fog_directory = ENV['BUCKET_NAME']
  config.max_file_size = 4000000.megabytes 
end