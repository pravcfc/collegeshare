class AddIndexesPart1 < ActiveRecord::Migration
  def change
		#relations
		add_index :relations, :follower_id
	  add_index :relations, :followed_id
	  add_index :relations, [:follower_id, :followed_id], unique: true

	  #uploads
	  add_index :uploads, :user_id
	  add_index :uploads, [:id, :user_id], unique: true
	  add_index :uploads, [:name, :description]
	  add_index :uploads, [:name, :user_id, :description, :id], unique: true

	  #taggings
	  add_index :taggings, :upload_id
	  add_index :taggings, :tag_id

	  #tags
	  add_index :tags, :id

	  #comments
	  add_index :comments, :user_id
	  add_index :comments, :owner_id
  end
end
