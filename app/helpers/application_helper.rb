module ApplicationHelper
		def is_owner
			if ['uploads', 'interviews', 'questions', 'comments'].include? params[:controller]
				model = Object.const_get(params[:controller].singularize.capitalize).find params[:id]
				redirect_to root_path, alert: 'Can perform action only if owner!!' unless(current_user.owner?(model) or !model or current_user.admin?)
			end
		end

		def is_admin
	  	redirect_to root_path, alert: 'Only admin can perform this action!!' unless(current_user.admin? or !current_user)
	  end

	def college_set
		if current_user.college_id.nil?
			redirect_to profile_path(current_user), alert: 'Cannot perform action without choosing a College!!'
		end
	end
end
