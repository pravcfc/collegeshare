class AddVerifiedToUpload < ActiveRecord::Migration
  def change
  	add_column :uploads, :verified, :boolean, default: false
  end
end
