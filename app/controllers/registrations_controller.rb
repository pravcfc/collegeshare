class RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    @user = User.new(sign_up_params)
    if @user.save!
      sign_in(@user)
      redirect_to root_path, notice: 'Signed up succesfully!'
    else
      render 'new'
    end
  end

  def update
    super
  end

  private

  def sign_up_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :college_id)
  end
 
  def account_update_params
    params.require(:user).permit(:name, :email, :college_id, :password, :password_confirmation, :current_password)
  end
end 