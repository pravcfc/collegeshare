class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
	def all
		user = User.from_omniauth(request.env["omniauth.auth"])
		if user
			sign_in_and_redirect user, notice: 'Signed in successfully!'
		else
			redirect_to new_user_registration_path, alert: 'Failed to sign in, please try again!'
		end
	end

	alias_method :twitter, :all
	alias_method :facebook, :all
	alias_method :google_oauth2, :all
end