class AddUpvotesToUpload < ActiveRecord::Migration
  def change
  	add_column :uploads, :upvote, :integer, default: 0
  end
end
