class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  validates_presence_of :name, :email
  belongs_to :college
  has_many :uploads, dependent: :destroy
  has_many :downloads, dependent: :destroy
  has_many :upload_upvotes, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :relations, class_name: 'Relation', foreign_key: 'follower_id', dependent: :destroy
  has_many :following, through: :relations, source: :followed
  has_many :reverse_relations, class_name: 'Relation', foreign_key: 'followed_id', dependent: :destroy
  has_many :followers, through: :reverse_relations, source: :follower
  devise :database_authenticatable, :registerable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable

 def self.from_omniauth(auth)
    where(provider: auth["provider"], uid: auth["uid"]).first || create_from_omniauth(auth)
  end

  def self.create_from_omniauth(auth)
    new do |user|
      user.provider = auth["provider"]
      user.uid = auth["uid"]
      user.name = auth["info"]["name"]
      if auth["provider"] == 'google_oauth2'
        user.email = auth["info"]["email"]
        user.profile_pic = auth["info"]["image"].split('?').first + '?sz=512'
      elsif auth["provider"] == 'facebook'
        user.email = auth["info"]["email"]
        user.profile_pic = auth["info"]["image"].split('?').first + '?type=large'
      elsif auth["provider"] == 'twitter'
        user.email = auth["info"]["nickname"] + '@twitter.com'
        user.profile_pic = auth["info"]["image"].sub('_normal', '')
      else
        return nil
      end
      user.save!
    end
  end

  def password_required?
    super && provider.blank?
  end

  def update_with_password(params, *options)
    if encrypted_password.blank?
      update_attributes(params, *options)
    else
      super
    end
  end

  def owner?(model)
    model.has_attribute?(:user_id) ? model.user_id == self.id : nil
  end

  def college
    College.find self.college_id if self.college_id
  end

  def make_admin
    self.update_attributes(admin: true)
  end

  def follow(other_user)
    relations.create(followed_id: other_user.id)
  end

  def unfollow(other_user)
    relations.find_by(followed_id: other_user.id).destroy
  end

  def following?(other_user)
    following.include?(other_user)
  end
end