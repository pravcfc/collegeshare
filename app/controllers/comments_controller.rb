class CommentsController < ApplicationController
	before_action :authenticate_user!

	def create
		@comment = Comment.new(comment_params)
		@comment.user_id = current_user.id
		if @comment.save
			redirect_to :back, notice: 'Comment created succesfully!'
		else
			redirect_to :back, notice: 'Comment not created!'
		end
	end

	def destroy
		@comment = Comment.find(params[:id])
		if @comment.delete
			redirect_to :back, notice: 'Comment deleted succesfully!'
		else
			redirect_to :back, notice: 'Comment failed to delete!'
		end
	end

	private
	def comment_params
		params.require(:comment).permit(:content, :owner_id, :owner_type)
	end
end
