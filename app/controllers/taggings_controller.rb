class TaggingsController < ApplicationController
	def show 
		@tag = Tag.find params[:id]
		@taggings = Tagging.where(tag_id: params[:id])
		@uploads = @taggings.map{ |tagging| Upload.find(tagging.upload_id) }
	end
end