class MakeCommentsPolymorphic < ActiveRecord::Migration
  def change
  	add_column :comments, :owner_id, :integer
  	add_column :comments, :owner_type, :string
  	remove_column :comments, :interview_id
  end
end
