class ChangeToNoOfDownloads < ActiveRecord::Migration
  def change
  	rename_column :uploads, :downloads, :no_of_downloads
  end
end
